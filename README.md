TTCP is a benchmarking tool for determining TCP and UDP performance 
between 2 systems.


The program was created at the US Army Ballistics Research Lab (BRL).
Feel free to distribute this program but please do leave the credit
notices in the source and man page intact.


Contents of this directory:

```ttcp.c		Source that runs on IRIX 3.3.x and 4.0.x systems
		and BSD-based systems.  This version also uses getopt(3) 
		and has 2 new options: -f and -T.
```

```ttcp.c-brl	Original source from BRL.
```
```ttcp.1		Manual page (describes ttcp.c options, which are a 
		superset of the other version).
```

How to get TCP performance numbers:
```
	receiver				sender
```
```
host1%  ttcp -r -s			host2% ttcp -t -s host1
```
```-n and -l options change the number and size of the buffers.
```


# The Story of the TTCP Program

Along with PING, there are a few other fun bits of network code that I got to write
back in the early days of TCP/IP. 


## Fun Network Hacks

Some other bits of kernel code that I've originated include the "default route"
support, which a lot of people depend on to get their packets to an InterNet router
when the full generality of a dynamic routing protocol is not required, or isn't
working.


I also devised the "TCP max segment size (MSS) follows departing interface maximum
transmission unit (MTU)" algorithm, which greatly improved TCP/IP efficiency in the
face of dropped datagrams by allowing TCP to avoid using IP fragmentation.


I then further extended the algorithm to bring BSD UNIX into strict conformance with
the TCP specification, and limit the TCP segment size when transmitting to faraway
systems to 576 bytes. This is the origin of the "subnets-are-local" flag, which
sometimes frustrates LAN sys-admins, but allowed mail to flow to Multics and Univac
machines which (back then) adhered to the letter of the specification and could only
handle 512 byte TCP segments. Since at the time I was the moderator of the
[TCP-IP Digest](http://ftp.arl.army.mil/ftp/tcp-ip-digest/), the Unix-Wizards Digest
and the INFO-UNIX Digest, I had a lot of packets to send to Multics and Univac
machines, and the mail had to go through.

## The TTCP Program

Along with Terry Slattery (then of the US Naval Academy), we took inspiration from
an (Excelan? Interlan?) network test program, and evolved it into a program called
TTCP, to "Test TCP". In addition to performing its intended function of testing TCP
performance from user-memory to user-memory, TTCP has remained an excellent tool for
bootstrapping hosts onto the network, by providing (essentially) a UNIX "pipe"
between two machines across the network. For example, on the destination machine,
use:
```
    ttcp -r | tar xvpf -
```
and on the source achine:
```
    tar cf - directory | ttcp -t dest_machine
```
To work around routing problems, additional intermediate machines can be included
by:
```
    ttcp -r | ttcp -t next_machine
```

TTCP has become another standard UNIX networking tool.

---
More about [Mike](http://ftp.arl.army.mil/~mike/) (one of the original authors) may
he RIP. `:'(`
