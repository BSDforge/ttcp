````
This is more a list of notes, than a changelog proper. Perhaps now that
it's in a modern VCS that will change? No promises. ;)

* Modified for operation under 4.2BSD, 18 Dec 84
      T.C. Slattery, USNA
* Minor improvements, Mike Muuss and Terry Slattery, 16-Oct-85.
* Modified in 1989 at Silicon Graphics, Inc.
	catch SIGPIPE to be able to print stats when receiver has died 
	for tcp, don't look for sentinel during reads to allow small transfers
	increased default buffer size to 8K, nbuf to 2K to transfer 16MB
	moved default port to 5001, beyond IPPORT_USERRESERVED
	make sinkmode default because it is more popular, 
		-s now means don't sink/source 
	count number of read/write system calls to see effects of 
		blocking from full socket buffers
	for tcp, -D option turns off buffered writes (sets TCP_NODELAY sockopt)
	buffer alignment options, -A and -O
	print stats in a format that's a bit easier to use with grep & awk
	for SYSV, mimic BSD routines to use most of the existing timing code
* Modified by Steve Miller of the University of Maryland, College Park
	-b sets the socket buffer size (SO_SNDBUF/SO_RCVBUF)
* Modified Sept. 1989 at Silicon Graphics, Inc.
	restored -s sense at request of tcs@brl
* Modified Oct. 1991 at Silicon Graphics, Inc.
	use getopt(3) for option processing, add -f and -T options.
	SGI IRIX 3.3 and 4.0 releases don't need #define SYSV.
* A mess of changes too large to recall. Largely to track changes to FreeBSD'
    kernel -- network stack, specifically. Then those necessary to cope with
    changes in modern day compilers.
